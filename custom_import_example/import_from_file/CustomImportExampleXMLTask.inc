<?php
/**
 * @file
 * Created by Neurones
 * User: ogneurones
 * Date: 15/11/13
 * Time: 08:13
 */

class CustomImportExampleXMLTask extends CustomImportFileTask {

  /**
   * Get the content to import and put it in $this->content.
   */
  protected function getContent() {
    $this->content = @file_get_contents($this->getFilePath());
  }

  /**
   * Parse the content with a parser (ex: XMLParser), put it in $this->data.
   */
  protected function parseContent() {
    $parser = new CustomImportExampleXMLParser($this->content);
    $this->data = $parser->getParsedData();
  }

  /**
   * Get an importer object (ex: new NeuronesNodeImport).
   *
   * @param array $content_type
   *   bundle name of content.
   *
   * @return CustomImportImporter
   *   An importer object.
   */
  protected function getImporter($content_type) {
    $importer = NULL;
    switch ($content_type) {
      case 'page':
        $importer = new CustomImportExampleNodeImporter($content_type, $this->data);
        break;

      case 'tags':
        $importer = new CustomImportExampleTaxonomyImporter($content_type, $this->data);
        break;

    }
    return $importer;
  }
}
