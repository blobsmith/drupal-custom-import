<?php
/**
 * @file
 * Created by Neurones
 * User: ogneurones
 * Date: 03.01.14
 * Time: 14:34
 */

class CustomImportExampleTaxonomyImporter extends CustomImportTaxonomyImporter {

  /**
   * Get the line reference of the object to import.
   *
   * @param array $line
   *   list of data in a line to import.
   *
   * @return string
   *   line reference.
   */
  protected function getReference($line) {
    return $line[CustomImportExampleXMLParser::getRefFieldName()];
  }

  /**
   * Get the language id for save to Drupal (en, fr...).
   *
   * @param array $line
   *   list of data in a line to import.
   *
   * @return string
   *   code language.
   */
  protected function getLanguage($line) {
    return LANGUAGE_NONE;
  }

  /**
   * Called before saving object to Drupal.
   *
   * @param StdClass $term
   *   a Drupal object taxonomy_term.
   * @param array $line
   *   list of data in a line to import.
   * @param mixed $line_number
   *   number of the imported line.
   *
   * @return StdClass
   *   modified Drupal object taxonomy_term.
   */
  protected function beforeSave($term, $line, $line_number) {
    $term->name = $line['name'];
    return $term;
  }
}
