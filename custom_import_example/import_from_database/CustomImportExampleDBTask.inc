<?php
/**
 * @file
 * Created by Neurones
 * User: ogneurones
 * Date: 15/11/13
 * Time: 08:13
 */

class CustomImportExampleDBTask extends CustomImportSQLTask {

  /**
   * Get the content to import and put it in $this->content.
   */
  protected function getContent() {
    $sql = "SELECT e.id, e.name, e.text FROM {exemple_custom_import_data} e";
    $this->content = $this->dbQuery($sql);
  }

  /**
   * Parse the content with a parser.
   */
  protected function parseContent() {
    $parser = new CustomImportExampleSQLParser($this->content);
    $this->data = $parser->getParsedData();
  }

  /**
   * Get an importer object (ex: new NeuronesNodeImport).
   *
   * @param array $content_type
   *   bundle name of content.
   *
   * @return CustomImportImporter
   *   An importer object.
   */
  protected function getImporter($content_type) {
    return new CustomImportExampleArticleImporter($content_type, $this->data);
  }
}
