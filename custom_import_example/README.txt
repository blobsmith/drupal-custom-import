********************************************************************************
*                               CUSTOM IMPORT MODULE EXAMPLE
********************************************************************************

- Add the code commented below in your settings.php configuration file

$conf['custom_import'] = array(               //module custom import settings
    'CustomImportExampleXMLTask' => array(    //class name to set the settings
        'filePath' => 'public://import/monthToTags.xml',//path to file to import
        'bundleName' => array('page','tags'), //Content machine name for import
                                              //content in node basic page
                                              //and taxonomy tags
        'memoryLimit' => '128M'               //memory usage
    ),
    'CustomImportExampleDBTask' => array(     //class name to set the settings
        'db' => array(                        //Set external database settings
          'driver' => 'mysql',
          'host' => 'localhost',
          'username' => 'root',
          'password' => '',
          'database' => 'neurones_import',
        ),
        'bundleName' => array('article'),     //Content machine name for import
      ),
);

- Copy the file monthToTags.xml
  from custom_import/custom_import_example/monthToTags.xml
  to public://import/monthToTags.xml
- Change db settings and put, for this example the same settings as Drupal
- Install the module custom_import_example
- Go to admin/custom_imports:
  * click on "Example of node and ta.." to import the Excel xml Spreadsheet file
  * click on "Example of node article.." to import the database data contain
    in table "exemple_custom_import_data"

  The 4 tasks call by Drupal batch are:
  -> getExistingContent:        //get the content already imported in Drupal
  -> updateContent:             //create or update the content
  -> removeContent:             //remove the content from diff between
                                //imported data and Drupal data
  -> writeLogs:                 //write logs for get information from import

- At the end of the import, you can click on the log files to see the import
  details (refresh logs page if it's not the first time...)
- Got to admin/content, you can see 12 basic pages imported
- Got to admin/structure/taxonomy/tags, you can see 12 tags imported
