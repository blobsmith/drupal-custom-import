<?php
/**
 * @file
 * Created by Neurones
 * User: ogneurones
 * Date: 14/11/13
 * Time: 14:43
 */

class CustomImportCSVTestParser extends CustomImportCSVParser {

  /**
   * Get the reference column name.
   *
   * @return mixed
   *   the key name
   */
  public static function getRefFieldName() {
    return 'number';
  }
}
