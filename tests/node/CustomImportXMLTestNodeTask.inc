<?php
/**
 * @file
 * Created by Neurones
 * User: ogneurones
 * Date: 15/11/13
 * Time: 08:13
 */

class CustomImportXMLTestNodeTask extends CustomImportFileTask {

  /**
   * Parse the content with a parser (ex: IDXParser), put it in $this->data.
   */
  protected function parseContent() {
    $parser = new CustomImportXMLTestNodeParser($this->content);
    $this->data = $parser->getParsedData(';');
  }

  /**
   * Get an importer object (ex: new NeuronesNodeImport).
   *
   * @param array $content_type
   *   bundle name of content.
   *
   * @return CustomImportImporter
   *   An importer object.
   */
  protected function getImporter($content_type) {
    return new CustomImportTestNodeImport($content_type, $this->data);
  }

  /**
   * Get the content to import and put it in $this->content.
   */
  protected function getContent() {
    $this->content = file_get_contents($this->getFilePath());
  }
}
