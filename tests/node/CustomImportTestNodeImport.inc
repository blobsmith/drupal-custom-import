<?php
/**
 * @file
 * Created by Neurones
 * User: ogneurones
 * Date: 03.01.14
 * Time: 14:34
 */

class CustomImportTestNodeImport extends CustomImportNodeImporter {

  /**
   * Get the line reference of the object to import.
   *
   * @param array $line
   *   list of data in a line to import.
   *
   * @return string
   *   line reference.
   */
  protected function getReference($line) {
    return $line[CustomImportXMLTestNodeParser::getRefFieldName()];
  }

  /**
   * Get the language id for save to Drupal (en, fr...).
   *
   * @param array $line
   *   list of data in a line to import.
   *
   * @return string
   *   code language.
   */
  protected function getLanguage($line) {
    return LANGUAGE_NONE;
  }

  /**
   * Called before saving object to Drupal.
   *
   * @param StdClass $node
   *   an object (node, taxonomy...).
   * @param array $line
   *   list of data in a line to import.
   * @param mixed $line_number
   *   number of the imported line.
   *
   * @return StdClass
   *   modified $node.
   */
  protected function beforeSave($node, $line, $line_number) {
    $node->title = $line['name'];
    $node->body[LANGUAGE_NONE][0]['value'] = $line['content'];
    return $node;
  }
}
