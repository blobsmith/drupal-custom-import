<?php
/**
 * @file
 * Created by Neurones
 * User: ogneurones
 * Date: 25.11.13
 * Time: 10:59
 */

/**
 * Class CustomImportLogger
 */
class CustomImportLogger {
  protected $debugMode = FALSE;
  protected $logFile = NULL;
  protected $logs = array();
  protected $importKey = NULL;

  /**
   * Constructor.
   *
   * @param string $import_key
   *   Key name used for the file name.
   */
  public function __construct($import_key) {
    $this->importKey = $import_key;
    if (!preg_match('#[0-9a-zA-Z]#', $this->importKey)) {
      throw new Exception('The import key "' . $this->importKey . '" do not match with CustomImportLogger conditions.');
    }
    $this->logFile = self::getLogFileName($this->importKey);
  }

  /**
   * Get the log file name.
   *
   * @param string $import_key
   *   Key name used for the file name.
   *
   * @return string
   *   Log file name.
   */
  public static function getLogFileName($import_key) {
    return 'log_' . $import_key . '.html';
  }

  /**
   * Get the directory log file path.
   *
   * @return string
   *   Log file path.
   */
  public static function getLogFilePath() {
    $file_path = 'public://import';

    // Make directory if needed.
    if (!file_prepare_directory($file_path)) {
      drupal_mkdir($file_path);
    }
    return $file_path;
  }

  /**
   * Get the log file path.
   *
   * @param bool $url
   *   Path in url format or server file format.
   *
   * @return string
   *   File path.
   */
  public static function getLogFile($import_key, $url = TRUE) {
    $file_name = self::getLogFileName($import_key);
    $file_path = self::getLogFilePath();
    if ($url) {
      $file_path = file_create_url($file_path) . '/' . $file_name;
    }
    else {
      $file_path = drupal_realpath($file_path) . DIRECTORY_SEPARATOR . $file_name;
    }
    return $file_path;
  }

  /**
   * Put a log message in memory.
   *
   * @param string $message
   *   Message to set in logs.
   */
  public function setLog($message) {
    $this->logs[] = $message;

    if ($this->debugMode) {
      var_dump($message);
    }
  }

  /**
   * Create a log file and put all logs inside.
   */
  public function saveLogs() {
    $file_path = self::getLogFile($this->importKey, FALSE);
    $content = implode("<br />", $this->logs);
    file_put_contents($file_path, $content);
  }

  /**
   * Finish work by setting a Drupal update date and saving logs in file.
   *
   * @param int $total_elements
   *   Total elements in Drupal.
   * @param timestamp $start_date
   *   Task start date.
   */
  public function finish($total_elements = 0, $start_date = NULL) {
    $this->setLog(t('Total elements in Drupal: %total', array(
        '%total' => $total_elements,
        )));

    if ($start_date !== NULL) {
      $this->getImportDuration($start_date);
    }
    $this->saveLogs();
  }

  /**
   * Set the task duration.
   *
   * @param string $start_date
   *   Task start date (sql format).
   */
  public function getImportDuration($start_date) {
    $timestamp_start_date = strtotime($start_date);
    $end_date = format_date(time(), 'custom', 'Y-m-d H:i:s');
    $timestamp_end_date = strtotime($end_date);

    $this->setLog(t('Start date: %date', array(
      '%date' => date('d/m/Y H:i:s', $timestamp_start_date),
    )));

    $date_from = new DateTime();
    $date_from->setTimestamp($timestamp_start_date);
    $date_now = new DateTime();
    $date_now->setTimestamp($timestamp_end_date);
    $interval = $date_now->diff($date_from);
    $this->setLog(t('Import duration: %minutes minutes %seconds seconds', array(
        '%minutes' => $interval->format('%i'),
        '%seconds' => $interval->format('%s'),
      )));
  }

  /**
   * Set the debug mode, true to display logs.
   *
   * @param bool $debug_mode
   *   Debug mode boolean.
   */
  public function setDebugMode($debug_mode) {
    $this->debugMode = $debug_mode;
  }
}
