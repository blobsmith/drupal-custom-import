<?php
/**
 * @file
 * Created by Neurones
 * User: ogneurones
 * Date: 14/11/13
 * Time: 14:43
 */

abstract class CustomImportSQLParser extends CustomImportParser {

  /**
   * This function define how to parse data for each specific type of data.
   *
   * @return array
   *   All data parsed in $this->parsed_data.
   */
  protected function extractData() {
    if (!$this->data) {
      throw new Exception(t('No data can be found'));
    }

    $this->parsedData = array();

    $lines = $this->data->fetchAll();
    foreach ($lines as $line) {
      $this->parsedData[] = (array) $line;
    }
  }
}
