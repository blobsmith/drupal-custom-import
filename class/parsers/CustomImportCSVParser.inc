<?php
/**
 * @file
 * Created by Neurones
 * User: ogneurones
 * Date: 14/11/13
 * Time: 14:43
 */

abstract class CustomImportCSVParser extends CustomImportParser {
  protected $endOfLine = NULL;
  protected $delimiter = NULL;
  protected $enclosure = NULL;
  protected $escape = NULL;

  /**
   * Constructor.
   *
   * @param array $data
   *   Data to parsed.
   * @param string $end_of_line
   *   Character to define a end of line.
   */
  public function __construct($data, $end_of_line = "\n") {
    $this->endOfLine = $end_of_line;
    parent::__construct($data);
  }


  /**
   * Get all parsed data.
   *
   * @return array
   *   Array of data.
   */
  public function getParsedData($delimiter = ',', $enclosure = '"', $escape = '\\') {
    $this->delimiter = $delimiter;
    $this->enclosure = $enclosure;
    $this->escape = $escape;
    if (empty($this->parsedData)) {
      $this->extractData();
    }
    return $this->parsedData;
  }

  /**
   * This function define how to parse data for each specific type of data.
   *
   * @return array
   *   All data parsed in $this->parsed_data.
   */
  protected function extractData() {
    if (empty($this->data)) {
      throw new Exception('No data can be found');
    }
    $lines = explode($this->endOfLine, $this->data);

    // One line for header and one line for content.
    if (count($lines) < 2) {
      throw new Exception('No lines of data can be found');
    }

    $header_line = array_shift($lines);
    $header = str_getcsv($header_line, $this->delimiter, $this->enclosure, $this->escape);

    foreach ($lines as $line_number => $line) {
      $exploded_data = str_getcsv($line, $this->delimiter, $this->enclosure, $this->escape);
      foreach ($exploded_data as $key => $data) {
        if (isset($header[$key])) {
          $this->parsedData[$line_number][$header[$key]] = $data;
        }
      }
    }
  }
}
