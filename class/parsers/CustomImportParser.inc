<?php
/**
 * @file
 * Created by Neurones
 * User: ogneurones
 * Date: 06.01.14
 * Time: 08:14
 */

interface CustomImportParserInterface {

  /**
   * Get all parsed data.
   *
   * @return array
   *   Array of data
   */
  public function getParsedData();

  /**
   * Get reference column name corresponding of one key name.
   *
   * @return mixed
   *   the key name
   */
  public static function getRefFieldName();
}

/**
 * Class CustomImportParser
 *
 * $data: Original not parsed data.
 * $parsedData: Parsed data: array filled by extractData.
 */
abstract class CustomImportParser implements CustomImportParserInterface {
  protected $data = array();
  protected $parsedData = array();

  /**
   * Constructor.
   *
   * @param array $data
   *   Data to parsed.
   */
  public function __construct($data) {
    $this->data = $data;
  }

  /**
   * Get all parsed data.
   *
   * @return array
   *   Array of data.
   */
  public function getParsedData() {
    if (empty($this->parsedData)) {
      $this->extractData();
    }
    return $this->parsedData;
  }

  /**
   * This function define how to parse data for each specific type of data.
   *
   * @return array
   *   All data parsed in $this->parsed_data.
   */
  abstract protected function extractData();

}
