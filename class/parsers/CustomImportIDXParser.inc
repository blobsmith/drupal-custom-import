<?php
/**
 * @file
 * Created by Neurones
 * User: ogneurones
 * Date: 14/11/13
 * Time: 14:43
 */

/**
 * Class CustomImportIDXParser
 *
 * $dataDefinitions: Used for change the numerical keys by text keys.
 */
abstract class CustomImportIDXParser extends CustomImportParser {
  protected $dataDefinitions = array();

  /**
   * Constructor.
   *
   * @param array $data
   *   Data to parsed.
   */
  public function __construct($data) {
    parent::__construct($data);
    $this->loadDataDefinitions();
  }

  /**
   * This function define how to parse data for each specific type of data.
   *
   * @return array
   *   All data parsed in $this->parsed_data.
   */
  protected function extractData() {
    if (empty($this->data)) {
      throw new Exception('No data can be found');
    }

    $lines = explode("\n", $this->data);
    if (count($lines) === 0) {
      throw new Exception('No lines of data can be found');
    }

    $proterty_number = 0;
    foreach ($lines as $line) {
      $exploded_data = explode('#', $line);
      foreach ($exploded_data as $key => $data) {
        if (isset($this->dataDefinitions[$key])) {
          $this->parsedData[$proterty_number][$this->dataDefinitions[$key]] = $data;
        }
      }
      $proterty_number++;
    }
  }


  /**
   * Define a key name for each column number.
   */
  abstract protected function loadDataDefinitions();

  // ex: protected function loadDataDefinitions() {
  // ex:   $this->data_definitions = array(
  // ex:     0 => 'title',   //Just an example
  // ex:     //....
  // ex:   );
  // ex:   $this->data_definitions;
  // ex: }
}
