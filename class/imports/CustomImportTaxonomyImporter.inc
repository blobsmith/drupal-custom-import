<?php
/**
 * @file
 * Created by Neurones
 * User: ogneurones
 * Date: 20.11.13
 * Time: 11:46
 */

abstract class CustomImportTaxonomyImporter extends CustomImportImporter {
  protected $entityType = 'taxonomy_term';
  protected $vocabulary = NULL;

  /**
   * Constructor.
   *
   * @param string $taxonomy_machine_name
   *   Taxonomy type (bundle name)
   * @param array $lines_to_import
   *   List of data to import.
   */
  public function __construct($taxonomy_machine_name, $lines_to_import) {
    parent::__construct($taxonomy_machine_name, $lines_to_import);
    $this->vocabulary = taxonomy_vocabulary_machine_name_load($this->contentMachineName);
  }

  /**
   * Get all the already imported existing content.
   *
   * @param array $data
   *   data passed to the function
   * @param array $context
   *   context used with Drupal batch
   */
  public function getExistingContent($data, &$context) {
    parent::getExistingContent($data, $context);
    if (isset($this->existingContentIds)) {
      $data = array_chunk(array_keys($this->existingContentIds), CustomImportLoader::CONTENT_LOADING_NUMBER);
      foreach ($data as $ids) {
        $terms = taxonomy_term_load_multiple($ids, array('vid' => $this->vocabulary->vid));
        $context['message'] = t('loading terms %bundleName %countMin to %coutMax', array(
            '%bundleName' => $this->contentMachineName,
            '%countMin' => $context['sandbox']['progress'],
            '%coutMax' => $context['sandbox']['progress'] + count($ids),
          )
        );
        foreach ($terms as $term) {
          $ref = field_get_items('taxonomy_term', $term, CustomImportTools::FIELD_REF);
          if ($ref !== FALSE) {
            $this->existingContent[$ref[0]['value']] = $term;
            $context['sandbox']['progress']++;
          }
        }
      }
    }
    $context['finished'] = 1;
  }

  /**
   * Create or update each content from lines to import.
   *
   * @param array $data
   *   data passed to the function
   * @param array $context
   *   context used with Drupal batch
   */
  public function updateContent($data, &$context) {
    parent::updateContent($data, $context);

    if ($this->dataLinesToImport === NULL || empty($this->dataLinesToImport)) {
      $context['results']['messages'][] = t('Taxonomy Import: error, No data found.');
    }
    else {
      // Name for fields in Drupal.
      $field_imported_name = CustomImportTools::FIELD_IMPORTED;
      $field_ref_name = CustomImportTools::FIELD_REF;

      foreach ($this->dataLinesToImport as $line_number => $line) {
        $term = NULL;
        $ref = $this->getReference($line);
        if ($ref === NULL || $ref === '') {
          continue;
        }
        $message = '';

        if (!isset($this->existingContent[$ref])) {
          // Create.
          $term = new stdClass();
          $term->vid = $this->vocabulary->vid;
          $term->field_ref[LANGUAGE_NONE][0]['value'] = $ref;

          $term->$field_imported_name = array(
            LANGUAGE_NONE => array(
              array(
                'value' => self::IMPORTED_VALUE,
              ),
            ),
          );
          $term->$field_ref_name = array(
            LANGUAGE_NONE => array(
              array(
                'value' => $ref,
              ),
            ),
          );
          $message = 'created';
        }
        else {
          // Update.
          $term = $this->existingContent[$ref];
          $message = 'updated';
        }

        $term = $this->beforeSave($term, $line, $line_number);
        $this->save($term, $line, $line_number);

        if (isset($term->tid)) {
          if ($message === 'created') {
            $context['results']['created']++;
          }
          else {
            $context['results']['updated']++;
          }
          $context['sandbox']['progress']++;
          $context['results']['messages'][] = t('Term %bundleName: %tid %message', array(
              '%bundleName' => $this->contentMachineName,
              '%tid' => $term->tid,
              '%message' => $message,
            ));
        }
        $term = $this->afterSave($term, $line, $line_number);
      }
    }
  }

  /**
   * Called after saving object to Drupal.
   *
   * @param StdClass $term
   *   An object (node, taxonomy...).
   * @param array $line
   *   List of data in a line to import.
   * @param mixed $line_number
   *   Number of the line.
   *
   * @return StdClass
   *   Modified $object.
   */
  protected function afterSave($term, $line, $line_number) {
    if (isset($term->tid)) {
      $ref = $this->getReference($line);
      $term->checked = TRUE;
      $this->existingContent[$ref] = $term;
    }
    return $term;
  }

  /**
   * Save the object extract from $line to Drupal.
   *
   * @param StdClass $term
   *   An object (node, taxonomy...).
   * @param array $line
   *   List of data in a line to import.
   * @param mixed $line_number
   *   Number of the line.
   *
   * @return StdClass
   *   Modified $object.
   */
  protected function save(&$term, $line, $line_number) {
    taxonomy_term_save($term);
  }

  /**
   * Remove existing content if it was not created or updated.
   *
   * @param array $data
   *   data passed to the function
   * @param array $context
   *   context used with Drupal batch
   */
  public function removeContent($data, &$context) {
    parent::removeContent($data, $context);
    foreach ($this->contentToBeDeleted as $tid) {
      taxonomy_term_delete($tid);

      $context['results']['messages'][] = t('Term %bundleName: %tid removed', array(
        '%bundleName' => $this->contentMachineName,
        '%tid' => $tid,
      ));
      $context['sandbox']['progress']++;
      $context['results']['removed']++;
    }

    $context['finished'] = 1;
  }
}
