<?php
/**
 * @file
 * Created by Neurones
 * User: ogneurones
 * Date: 20.11.13
 * Time: 11:46
 */

abstract class CustomImportNodeImporter extends CustomImportImporter {
  protected $entityType = 'node';

  /**
   * Get all the already imported existing content.
   *
   * @param array $data
   *   data passed to the function
   * @param array $context
   *   context used with Drupal batch
   */
  public function getExistingContent($data, &$context) {
    parent::getExistingContent($data, $context);
    if (isset($this->existingContentIds)) {
      $data = array_chunk(array_keys($this->existingContentIds), CustomImportLoader::CONTENT_LOADING_NUMBER);
      foreach ($data as $ids) {
        $nodes = node_load_multiple($ids, array(
          'type' => $this->contentMachineName,
        ));
        $context['message'] = t('loading nodes %bundleName %countMin to %coutMax', array(
            '%bundleName' => $this->contentMachineName,
            '%countMin' => $context['sandbox']['progress'],
            '%coutMax' => $context['sandbox']['progress'] + count($ids),
          )
        );
        foreach ($nodes as $node) {
          $ref = field_get_items('node', $node, CustomImportTools::FIELD_REF);
          if ($ref !== FALSE) {
            $this->existingContent[$ref[0]['value']] = $node;
            $context['sandbox']['progress']++;
          }
        }
      }
    }
    $context['finished'] = 1;
  }

  /**
   * Create or update each content from lines to import.
   *
   * @param array $data
   *   data passed to the function
   * @param array $context
   *   context used with Drupal batch
   */
  public function updateContent($data, &$context) {
    parent::updateContent($data, $context);
    global $user;
    if ($this->dataLinesToImport === NULL || empty($this->dataLinesToImport)) {
      $context['results']['messages'][] = t('Node Import: error, No data found.');
    }
    else {
      // Name for fields in Drupal.
      $field_imported_name = CustomImportTools::FIELD_IMPORTED;
      $field_ref_name = CustomImportTools::FIELD_REF;

      foreach ($this->dataLinesToImport as $line_number => $line) {
        $ref = $this->getReference($line);
        if ($ref === NULL || $ref === '') {
          continue;
        }
        $message = '';
        $node = NULL;

        if (isset($this->existingContent[$ref])) {
          // Update.
          $node = $this->existingContent[$ref];
          $message = 'updated';
        }
        else {
          // Create.
          $node = new stdClass();
          $node->type = $this->contentMachineName;
          $node->$field_imported_name = array(
            LANGUAGE_NONE => array(
              array(
                'value' => self::IMPORTED_VALUE,
              ),
            ),
          );
          $node->$field_ref_name = array(
            LANGUAGE_NONE => array(
              array('value' => $ref,
              ),
            ),
          );
          $node->language = $this->getLanguage($line);
          $node->uid = $user->uid;
          $message = 'created';
        }

        $node = $this->beforeSave($node, $line, $line_number);
        $this->save($node, $line, $line_number);

        if (isset($node->nid)) {
          if ($message === 'created') {
            $context['results']['created']++;
          }
          else {
            $context['results']['updated']++;
          }
          $context['sandbox']['progress']++;
          $context['results']['messages'][] = t('Node %contentType: %nid %message', array(
              '%contentType' => $this->contentMachineName,
              '%nid' => $node->nid,
              '%message' => $message,
            ));
        }

        $node = $this->afterSave($node, $line, $line_number);
      }
    }
    $context['finished'] = 1;
  }

  /**
   * Called after saving object to Drupal.
   *
   * @param StdClass $node
   *   An object (node, taxonomy...).
   * @param array $line
   *   List of data in a line to import.
   * @param mixed $line_number
   *   Number of the line.
   *
   * @return StdClass
   *   Modified $object.
   */
  protected function afterSave($node, $line, $line_number) {
    if (isset($node->nid)) {
      $ref = $this->getReference($line);
      $node->checked = TRUE;
      $this->existingContent[$ref] = $node;
    }
    return $node;
  }

  /**
   * Save the object extract from $line to Drupal.
   *
   * @param StdClass $node
   *   An object (node, taxonomy...).
   * @param array $line
   *   List of data in a line to import.
   * @param mixed $line_number
   *   Number of the line.
   *
   * @return StdClass
   *   Modified $object.
   */
  protected function save(&$node, $line, $line_number) {
    node_save($node);
  }

  /**
   * Remove existing content if it was not created or updated.
   *
   * @param array $data
   *   data passed to the function
   * @param array $context
   *   context used with Drupal batch
   */
  public function removeContent($data, &$context) {
    parent::removeContent($data, $context);
    foreach ($this->contentToBeDeleted as $nid) {
      node_delete($nid);

      $context['results']['messages'][] = t('Node %contentType: %nid removed', array(
        '%contentType' => $this->contentMachineName,
        '%nid' => $nid,
      ));
      $context['sandbox']['progress']++;
      $context['results']['removed']++;
    }

    $context['finished'] = 1;
  }
}
