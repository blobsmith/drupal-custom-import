<?php
/**
 * @file
 * Created by Neurones
 * User: ogneurones
 * Date: 15/11/13
 * Time: 08:13
 */

abstract class CustomImportTask {
  protected static $settings = NULL;
  protected $content = NULL;
  protected $data = NULL;
  protected $execLogs = array();
  protected $memoryLimit = '256M';

  /**
   * Constructor.
   */
  public function __construct() {
    $this->getSettings();
    ini_set('memory_limit', $this->memoryLimit);
    $this->getContent();
  }

  /**
   * Start the import of data.
   */
  public function import() {
    if (!$this->content || empty($this->content)) {
      throw new Exception(t("%class::import, No content available", array(
        '%class' => get_class($this),
      )));
    }
    if (!isset(self::$settings['bundleName'])) {
      throw new Exception(t('%class::import, A key "bundleName" must be define in your setting file.', array(
        '%class' => get_class($this),
      )));
    }

    $this->parseContent();
    if (isset(self::$settings['useBatch']) && self::$settings['useBatch'] === FALSE) {
      $this->exec();
    }
    else {
      $this->importData();
    }
  }

  /**
   * Check if the data loader service is available.
   */
  public function checkService() {
    if (!$this->content || empty($this->content)) {
      throw new Exceptiont(t("%class::checkService, No content available", array(
        '%class' => get_class($this),
      )));
    }
    return TRUE;
  }

  /**
   * Launch data importation without Drupal batch.
   */
  protected function exec() {
    $content_type = self::$settings['bundleName'];

    if (!is_array($content_type)) {
      $content_type = array($content_type);
    }

    foreach ($content_type as $type) {
      $import = $this->getImporter($type);
      if ($import) {
        $logs = $import->exec();
        $this->execLogs[get_class($import)] = $logs;
      }
    }
  }

  /**
   * Get logs when called exec function.
   *
   * @return array
   *   All logs.
   */
  protected function getExecLogs() {
    return $this->execLogs;
  }

  /**
   * Launch data importation with Drupal batch.
   */
  protected function importData() {
    $operations = array();
    $content_type = self::$settings['bundleName'];

    if (!is_array($content_type)) {
      $content_type = array($content_type);
    }

    foreach ($content_type as $type) {
      $import = $this->getImporter($type);
      if ($import) {
        $operations = array_merge($operations, $import->getOperations());
      }
    }
    $this->callBatch($operations);
  }

  /**
   * Call drupal batch and launch the process.
   *
   * @param array $operations
   *   Array of operations to be performed
   */
  protected function callBatch($operations) {
    $batch = array(
      'operations' => $operations,
      'finished' => 'custom_import_batchEnd',
      'title' => t('Importing content'),
      'error_message' => t('The import has encountered an error.'),
      'init_message' => t('Starting import.'),
      'progress_message' => t('Processed @current out of @total.'),
      'file' => drupal_get_path('module', 'custom_import') . '/class/task/CustomImportTask.inc',
    );

    batch_set($batch);
    batch_process(CustomImportLoader::MENU_URL);
  }

  /**
   * Get settings from settings.php file.
   */
  protected function getSettings() {
    self::$settings = CustomImportLoader::loadSettings(get_class($this));

    if (isset(self::$settings['memoryLimit'])) {
      $this->memoryLimit = self::$settings['memoryLimit'];
    }
  }

  /**
   * Get the content to import and put it in $this->content.
   */
  abstract protected function getContent();

  /**
   * Parse the content with a parser (ex: IDXParser) and put it in $this->data.
   */
  abstract protected function parseContent();

  /**
   * Get an importer object (ex: new NeuronesNodeImport).
   *
   * @param array $content_type
   *   Array of content types.
   *
   * @return CustomImportImporter
   *   An importer object.
   */
  abstract protected function getImporter($content_type);
}

/**
 * Ending the Drupal import batch.
 *
 * @param array $success
 *   Array of data for success.
 * @param array $results
 *   Array of results.
 * @param array $operations
 *   Array of operations on failed.
 */
function custom_import_batchEnd($success, $results, $operations) {
  if ($success) {
    drupal_set_message(t('The import has been performed.'));
    drupal_set_message(t('Log files list:'));
    foreach ($results['classList'] as $class_name) {
      drupal_set_message(l($class_name, CustomImportLogger::getLogFile($class_name), array(
        'attributes' => array(
          'target' => '_blank',
        ),
      )));
    }
  }
  else {
    drupal_set_message(t('An error occurred and processing did not complete.'), 'error');
  }
}
