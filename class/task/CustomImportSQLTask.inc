<?php
/**
 * @file
 * Created by Neurones
 * User: ogneurones
 * Date: 15/11/13
 * Time: 08:13
 */

abstract class CustomImportSQLTask extends CustomImportTask {
  protected $confDbKey = NULL;

  /**
   * Constructor.
   */
  public function __construct() {
    $this->getSettings();
    $this->confDbKey = get_class($this);
    ini_set('memory_limit', $this->memoryLimit);
    $this->dbConnect();
    $this->getContent();
  }

  /**
   * Get settings from settings.php file.
   *
   * @return array
   *   All settings for this class.
   */
  protected function getDBSettings() {
    if (!isset(self::$settings['db']) || !isset(self::$settings['db'])) {
      throw new Exception(t('%class::getDBSettings, A key ["db"] must be define in your setting file.', array(
        '%class' => get_class($this),
      )));
    }
    return self::$settings['db'];
  }

  /**
   * Connect to Sql Database.
   *
   * @return resource
   *   Resource to the database connection
   */
  protected function dbConnect() {
    $settings = $this->getDBSettings();
    if (!isset($settings['driver']) || !isset($settings['host']) || !isset($settings['username']) || !isset($settings['password']) || !isset($settings['database'])) {
      throw new Exception(t('%class::dbConnect, all keys "[db][driver]", "[db][host]", "[db][username]", "[db][password]", "[db][database]", must be defined in your setting file.', array('%class' => get_class($this))));
    }

    if ($settings['database'] === '') {
      throw new Exception(t('%class::dbConnect, the key "[db][database]" cannot be empty in your setting file.', array('%class' => get_class($this))));
    }

    $other_database = array(
      'database' => $settings['database'],
      'username' => $settings['username'],
      'password' => $settings['password'],
      'host' => $settings['host'],
      'driver' => $settings['driver'],
    );
    Database::addConnectionInfo($this->confDbKey, 'default', $other_database);
  }

  /**
   * Redefine Drupal db_query with the correct database selected.
   *
   * @param string $query
   *   The prepared statement query to run.
   * @param array $args
   *   An array of values to substitute into the query.
   * @param array $options
   *   An array of options to control how the query operates.
   *
   * @return array
   *   DatabaseStatementInterface A prepared statement object, already executed.
   */
  protected function dbQuery($query, array $args = array(), array $options = array()) {
    db_set_active($this->confDbKey);
    $results = db_query($query);
    db_set_active();
    return $results;
  }

}
